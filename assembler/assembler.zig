const std = @import("std");

const ProcessarError = error {
    InvalidArguments,
    InvalidInstruction,
    InvalidImmediateOperand,
    LabelAlreadyDefined,
    LabelContainsSpaces,
    LabelNotFound,
    MultipleCommas,
};

pub fn errToMsg(err: anyerror) [:0]const u8 {
    return switch (err) {
        ProcessarError.InvalidArguments => "",
        ProcessarError.InvalidInstruction => "",
        ProcessarError.InvalidImmediateOperand => "",
        ProcessarError.LabelAlreadyDefined => "",
        ProcessarError.LabelContainsSpaces => "",
        ProcessarError.LabelNotFound => "",
        ProcessarError.MultipleCommas => "",
        else => "Unknown error"
    };
}

pub inline fn isRegister(op: []const u8) bool {
    return op.len == 2 and op[1] == 'x' and op[0] >= 'a' and op[0] <= 'd';
}

pub inline fn getCodeReg(op: []const u8) u8 {
    if (!isRegister(op)) unreachable;
    return op[0] - 'a';
}

pub fn encodeAlu (
    inst_hex: []u8,
    inst_code1: u8,
    inst_code2: u8,
    op1: []const u8,
    op2: []const u8,
) !void {
    if (op2.len == 0 or !isRegister(op1)) {
        return ProcessarError.InvalidArguments;
    }

    if (isRegister(op2)) {
        inst_hex[0] = inst_code1 + if (inst_code1 > 9) 'a' - @as(u8, 10) else '0';
        const regs_num = 4 * getCodeReg(op1) + getCodeReg(op2);
        inst_hex[1] = regs_num + if (regs_num > 9) 'a' - @as(u8, 10) else '0';
        inst_hex[2] = 'f';
        inst_hex[3] = 'f';
    } else {
        inst_hex[0] = inst_code2 + if (inst_code2 > 9) 'a' - @as(u8, 10) else '0';

        const regs_num = 4 * getCodeReg(op1);
        inst_hex[1] = regs_num + if (regs_num > 9) 'a' - @as(u8, 10) else '0';

        const imm_num = try std.fmt.parseInt(u8, op2, 10);
        if (imm_num == std.math.maxInt(u8)) {
            return ProcessarError.InvalidImmediateOperand;
        }
        _ = try std.fmt.bufPrint(inst_hex[2 .. 4], "{x:0>2}", .{imm_num});
    }
}

pub fn encode(
    inst_hex: []u8,
    inst: []const u8,
    op1: []const u8,
    op2: []const u8,
    tab_labels: std.StringHashMap(usize)
) !void {
    if (std.mem.eql(u8, inst, "or")) {
        try encodeAlu(inst_hex, 0, 0, op1, op2);
    } else if (std.mem.eql(u8, inst, "not")) {
        if (op2.len != 0) {
            return ProcessarError.InvalidArguments;
        }

        try encodeAlu(inst_hex, 1, 1, op1, "ax");
    } else if (std.mem.eql(u8, inst, "and")) {
        try encodeAlu(inst_hex, 2, 2, op1, op2);
    } else if (std.mem.eql(u8, inst, "xor")) {
        try encodeAlu(inst_hex, 3, 3, op1, op2);
    } else if (std.mem.eql(u8, inst, "add")) {
        try encodeAlu(inst_hex, 4, 4, op1, op2);
    } else if (std.mem.eql(u8, inst, "sub")) {
        try encodeAlu(inst_hex, 5, 5, op1, op2);
    } else if (std.mem.eql(u8, inst, "mult")) {
        try encodeAlu(inst_hex, 6, 6, op1, op2);
    } else if (std.mem.eql(u8, inst, "div")) {
        try encodeAlu(inst_hex, 7, 7, op1, op2);
    } else if (std.mem.eql(u8, inst, "mov")) {
        try encodeAlu(inst_hex, 8, 9, op1, op2);
    } else if (std.mem.eql(u8, inst, "load")) {
        if (!isRegister(op2)) return ProcessarError.InvalidArguments;

        try encodeAlu(inst_hex, 10, 10, op1, op2);
    } else if (std.mem.eql(u8, inst, "store")) {
        if (!isRegister(op2)) return ProcessarError.InvalidArguments;

        try encodeAlu(inst_hex, 11, 11, op1, op2);
    } else if (std.mem.eql(u8, inst, "jmpz")) {
        if (op2.len != 0) return ProcessarError.InvalidArguments;

        if (isRegister(op1)) {
            try encodeAlu(inst_hex, 12, 12, op1, "ax");
        } else {
            const addr = tab_labels.get(op1) orelse
                return ProcessarError.LabelNotFound;

            inst_hex[0] = 'c';
            inst_hex[1] = '0';
            _ = try std.fmt.bufPrint(inst_hex[2 .. 4], "{x:0>2}", .{addr});
        }
    } else if (std.mem.eql(u8, inst, "jmpn")) {
        if (op2.len != 0) return ProcessarError.InvalidArguments;

        if (isRegister(op1)) {
            try encodeAlu(inst_hex, 13, 13, op1, "ax");
        } else {
            const addr = tab_labels.get(op1) orelse
                return ProcessarError.LabelNotFound;

            inst_hex[0] = 'd';
            inst_hex[1] = '0';
            _ = try std.fmt.bufPrint(inst_hex[2 .. 4], "{x:0>2}", .{addr});
        }
    } else if (std.mem.eql(u8, inst, "jmpp")) {
        if (op2.len != 0) return ProcessarError.InvalidArguments;

        if (isRegister(op1)) {
            try encodeAlu(inst_hex, 14, 14, op1, "ax");
        } else {
            const addr = tab_labels.get(op1) orelse
                return ProcessarError.LabelNotFound;

            inst_hex[0] = 'e';
            inst_hex[1] = '0';
            _ = try std.fmt.bufPrint(inst_hex[2 .. 4], "{x:0>2}", .{addr});
        }
    } else if (std.mem.eql(u8, inst, "halt")) {
        _ = try std.fmt.bufPrint(inst_hex[0 ..], "f000", .{});
    } else return ProcessarError.InvalidInstruction;
}

pub fn labelParser(
    allocator: *std.mem.Allocator,
    assm: std.fs.File,
    tab_labels: *std.StringHashMap(usize),
    line_count: *usize
) !void {
    try assm.seekTo(0);
    line_count.* = 1;

    const rdr = assm.reader();
    var buf = std.ArrayList(u8).init(allocator);
    defer buf.deinit();
    errdefer {
        var it = tab_labels.iterator();
        while (it.next()) |entry| {
            allocator.free(entry.key);
        }
    }

    while (true) {
        var ch: u8 = rdr.readByte() catch |err| switch (err) {
            error.EndOfStream => { return; },
            else => return err,
        };
        while (ch == ' ' or ch == '\t') {
            ch = rdr.readByte() catch |err| switch (err) {
                error.EndOfStream => { return; },
                else => return err,
            };
        }
        try assm.seekBy(-1);

        try rdr.readUntilDelimiterArrayList(&buf, '\n', 100);

        const tr_line = std.mem.trimRight(u8, buf.items, " \t");
        if (tr_line.len != 0 and tr_line[0] != '#') {
            // Check if line contains a label
            if (tr_line[tr_line.len - 1] == ':') {
                if (
                    std.mem.indexOfScalar(u8, tr_line, ' ') == null or
                    std.mem.indexOfScalar(u8, tr_line, '\t') == null
                ) {
                    buf.shrink(tr_line.len - 1);
                    const res = try tab_labels.getOrPut(buf.toOwnedSlice());
                    if (res.found_existing) {
                        return ProcessarError.LabelAlreadyDefined;
                    } else {
                        // TODO: Remove this
                        std.debug.print("{} label found\n", .{res.entry.key});

                        res.entry.value = line_count.*;
                    }
                } else {
                    return ProcessarError.LabelContainsSpaces;
                }
            } else {
                line_count.* += 1;
            }
        }
    } else |_| {
    }
}

pub fn instParserAndWriter(
    allocator: *std.mem.Allocator,
    assm: std.fs.File,
    hex: std.fs.File,
    tab_labels: std.StringHashMap(usize),
    line_count: *usize
) !void {
    try assm.seekTo(0);
    line_count.* = 1;

    const rdr = assm.reader();
    var buf = std.ArrayList(u8).init(allocator);
    defer buf.deinit();
    var inst_hex: [4]u8 = undefined;
    var inst_count: usize = 1;

    while (rdr.readUntilDelimiterArrayList(&buf, '\n', 100)) {
        var tr_line = std.mem.trim(u8, buf.items, " \t");

        if (tr_line.len != 0 and tr_line[0] != '#') {
            if (tr_line[tr_line.len - 1] != ':') {
                if (std.mem.indexOfScalar(u8, tr_line, '#')) |start| {
                    tr_line = std.mem.trimRight(
                        u8, tr_line[0 .. start], " \t"
                    );
                }

                const inst_end = std.mem.indexOfScalar(u8, tr_line, ' ') orelse
                    tr_line.len;
                const inst = tr_line[0 .. inst_end];

                const op1_end = std.mem.indexOfScalarPos(
                    u8, tr_line, inst_end, ','
                ) orelse tr_line.len;
                const op1 = std.mem.trim(u8, tr_line[inst_end .. op1_end], " ");

                if (std.mem.indexOfScalarPos(
                    u8, tr_line, op1_end + 1,  ','
                )) |_| {
                    return ProcessarError.MultipleCommas;
                }
                const op2 = std.mem.trim(u8, tr_line[op1_end ..], ", ");

                // TODO: Delete this
                std.debug.print("ins: {}, op1: {}, op2: {}\n", .{inst, op1, op2});

                try encode(inst_hex[0 ..], inst, op1, op2, tab_labels);

                try hex.writeAll(" ");
                try hex.writeAll(inst_hex[0 ..]);
            }
        }

        line_count.* += 1;
    } else |_| {
    }
}

pub fn processar(
    input_name: [:0]const u8,
    output_name: [:0]const u8,
    line_count: *usize
) !void {
    const cwd = std.fs.cwd();

    const assm = try cwd.openFile(input_name, .{});
    defer assm.close();

    const hex = try cwd.createFile(
        output_name,
        .{ .read = true }
    );
    defer hex.close();

    _ = try hex.writeAll("v2.0 raw\n0000");

    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    var allocator = &arena.allocator;

    var tab_labels = std.StringHashMap(usize).init(allocator);
    defer tab_labels.deinit();

    try labelParser(allocator, assm, &tab_labels, line_count);
    defer {
        var it = tab_labels.iterator();
        while (it.next()) |entry| {
            allocator.free(entry.key);
        }
    }
    try instParserAndWriter(allocator, assm, hex, tab_labels, line_count);
}

pub fn main() !void {
    var it = std.process.ArgIterator.init();
    _ = it.nextPosix();
    const input_name = it.nextPosix().?;
    const output_name = it.nextPosix() orelse "out.hex";
    var line_count: usize = undefined;

    processar(input_name, output_name, &line_count) catch |err| {
        const stderr = std.io.getStdErr().writer();
        try stderr.print("Error at line {}: {}.\n", .{line_count, errToMsg(err)});
        return err;
    };
}

const expect = std.testing.expect;
const expectEqualStrings = std.testing.expectEqualStrings;
const expectError = std.testing.expectError;

test "isRegisterTest" {
    expect(isRegister("ax"));
    expect(isRegister("bx"));
    expect(isRegister("cx"));
    expect(isRegister("dx"));
    expect(!isRegister("ex"));
    expect(!isRegister("axh"));
}

test "getCodeRegTest" {
    expect(getCodeReg("ax") == 0);
    expect(getCodeReg("bx") == 1);
    expect(getCodeReg("cx") == 2);
    expect(getCodeReg("dx") == 3);
}

test "encodeAluTest" {
    var inst_hex: [4]u8 = undefined;

    try encodeAlu(inst_hex[0 ..], 0, 0, "ax", "bx");
    expectEqualStrings("01ff", inst_hex[0 ..]);

    try encodeAlu(inst_hex[0 ..], 3, 3, "cx", "4");
    expectEqualStrings("3804", inst_hex[0 ..]);

    try encodeAlu(inst_hex[0 ..], 8, 9, "dx", "ax");
    expectEqualStrings("8cff", inst_hex[0 ..]);

    try encodeAlu(inst_hex[0 ..], 8, 9, "bx", "200");
    expectEqualStrings("94c8", inst_hex[0 ..]);

    try encodeAlu(inst_hex[0 ..], 11, 11, "cx", "dx");
    expectEqualStrings("bbff", inst_hex[0 ..]);

    const uni = encodeAlu(inst_hex[0 ..], 11, 11, "3", "dx");
    expectError(ProcessarError.InvalidArguments, uni);
}

test "encodeTest" {
    var inst_hex: [4]u8 = undefined;

    var tab_labels = std.StringHashMap(usize).init(std.testing.allocator);
    defer tab_labels.deinit();

    try encode(inst_hex[0 ..], "or", "ax", "bx", tab_labels);
    expectEqualStrings("01ff", inst_hex[0 ..]);

    try encode(inst_hex[0 ..], "jmpz", "cx", "", tab_labels);
    expectEqualStrings("c8ff", inst_hex[0 ..]);

    try encode(inst_hex[0 ..], "not", "bx", "", tab_labels);
    expectEqualStrings("14ff", inst_hex[0 ..]);

    try encode(inst_hex[0 ..], "halt", "", "", tab_labels);
    expectEqualStrings("f000", inst_hex[0 ..]);
}

test "labelParserTest" {
    const cwd = std.fs.cwd();

    const test_file = try cwd.createFile(
        "test_file.txt",
        .{ .read = true }
    );
    defer cwd.deleteFile("test_file.txt") catch
        std.debug.print("Could not delete file!\n", .{});
    defer test_file.close();

    try test_file.writeAll(
        \\alabel:
        \\blabel:
        \\clabel:
    );

    var tab_labels = std.StringHashMap(usize).init(std.testing.allocator);
    defer tab_labels.deinit();
    var line_count: usize = undefined;

    try labelParser(
        std.testing.allocator,
        test_file,
        &tab_labels,
        &line_count
    );
    var it = tab_labels.iterator();
    defer {
        it = tab_labels.iterator();
        while (it.next()) |entry| {
            std.testing.allocator.free(entry.key);
        }
    }

    while (it.next()) |entry| {
        std.debug.print("Found {} at line {}.\n", .{entry.key, entry.value});
    }

    expect(tab_labels.get("alabel").? == 1);
    expect(tab_labels.get("blabel").? == 1);
    expect(tab_labels.get("clabel").? == 1);

}
