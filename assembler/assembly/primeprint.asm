# Program to store prime numbers in data memory
mov ax, 2
mov cx, 0
loop1:
	mov dx, ax
	sub dx, 20 # All prime numbers before this number are stored
	jmpz endloop1
	jmpp endloop1

	mov bx, 2
	loop2:
		mov dx, bx
		sub dx, ax
		jmpz endloop2
		jmpp endloop2

		mov dx, ax
		div dx, bx
		mult dx, bx
		sub dx, ax
		jmpz endloop2

		mov dx, ax
		sub dx, bx
		sub dx, 1
		jmpp skip1
		jmpn skip1

		store cx, ax
		add cx, 1

		skip1:
		add bx, 1
		jmpp loop2

	endloop2:
	add ax, 1
	jmpp loop1

endloop1:
halt
